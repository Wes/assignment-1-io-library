section .text
%define EXIT_SYSCALL 60
%define STD_OUT 1
%define STD_IN 0
%define WRITE_SYSCALL 1
%define READ_SYSCALL 0
%define NEW_LINE_SYMBOL 0xA
%define SPACE ` `
%define TAB `\t`
%define MINUS_SIGN `-`
%define PLUS_SIGN `+`
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, EXIT_SYSCALL
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
		cmp byte[rdi+rax], 0	; проверка символа конца строки
		jz .break
		inc rax
		jmp .loop
	.break:
		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rdi, STD_OUT
	mov rax, WRITE_SYSCALL
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, NEW_LINE_SYMBOL

; Принимает код символа и выводит его в stdout
print_char:
    mov rdx, 1		; размер, 1 байт, в rdx
    push rdi		; сохраняем символ в стек
    mov rsi, rsp	; адрес выводимого символа в стеке помещаем в rsi
    mov rdi, STD_OUT     
    mov rax, WRITE_SYSCALL       
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
	jge print_uint
	push rdi
	mov rdi, MINUS_SIGN
	call print_char
	pop rdi
	neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    xor rdx, rdx
    mov r8, 10              
    mov r9, rsp	; сохраняем вершины стека
    dec rsp                      
    mov byte[rsp], 0	; нуль-терминатор
    .loop: 
        xor rdx, rdx                
        div r8
        add rdx, '0'	; первод в ASCII символ
        dec rsp                 
        mov byte[rsp],dl
        test rax, rax
        ja .loop
    mov rdi,rsp	
    push r9				; сохраняем вершины стека
    call print_string
    pop rsp				; возврат стека в исходное положение
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
		mov r8b, byte[rsi+rcx]
		cmp byte[rdi+rcx], r8b
		jne .end
		cmp byte[rdi+rcx], 0
		je .passed
		inc rcx
		jmp .loop
    .passed:
		mov rax, 1
		ret
	.end:
		xor rax, rax
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	sub	rsp, 8
	mov	rax, READ_SYSCALL
	mov	rdi, STD_IN 
	mov	rsi, rsp
	mov	rdx, 1
	syscall
	test    rax, rax
	jle	.return
	mov	rax, [rsp]
	.return:
		add	rsp, 8
		ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:     
    xor rax,rax
    mov rcx, 0          
    .loop:
        cmp rcx, rsi  
        jge .without_buffer  
        mov r12, rsi 
        mov r13, rdi  
        mov r14, rcx   
        call read_char  
        mov rcx, r14         
        mov rdi, r13 
        mov rsi, r12
        cmp rax, TAB   
        je .without_symbol       
        cmp rax, SPACE
        je .without_symbol
        cmp rax, NEW_LINE_SYMBOL
        je .without_symbol
        mov [rdi+rcx], rax 
        test rax, rax      
        je .ret         
        inc rcx         
        jmp .loop 
    .without_buffer:         
        xor rax, rax
        xor r12, r12
        xor r13, r13
        xor r14, r14
        ret     
    .without_symbol:
        test rcx, rcx    
        jne .ret       
        jmp .loop       
    .ret:            
        mov rdx, rcx
        mov rax, rdi
        xor r12, r12
        xor r13, r13
        xor r14, r14  
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
	mov rcx, 0 
    mov rsi, 0 
    mov r10, 10
    .loop:
    	movzx r9, byte[rdi+rcx]     
        sub r9, '0'                  
        test r9, r9   
        jl .ret 
        cmp r9, 9
        jg .ret 
        mul r10                     
        add rax, r9                 
        inc rcx                    
        jmp .loop 
    .ret:
		mov rdx, rcx
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov	al, [rdi]
	cmp	al, MINUS_SIGN
	je 	.negative
	cmp	al, PLUS_SIGN
	jne	.positive
	inc	rdi
	.positive:
		jmp	parse_uint
	.negative:
		inc	rdi
		call parse_uint
		neg	rax
		inc	rdx
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ; rsi - указывает на буффер, rdx - указывает на размер буффера, rdi - указывает на строку
     xor rax, rax               
    .loop:
        mov r9b, byte[rdi] ; копирование из строки в буффер при помощи промежуточного регистра r9b
        mov byte[rsi], r9b ; 
        cmp byte[rsi], 0x0  
        jz .passed
        inc rsi
        inc rdi
        inc rax                 
        cmp rax, rdx ; сравним длины строки и буффера
        jge .end
        jmp .loop
    .passed:
        ret 
    .end: 
        xor rax,rax             
        ret
